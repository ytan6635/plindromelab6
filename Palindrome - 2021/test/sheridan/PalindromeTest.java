/**
 * Yi Tan
 */
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean result= Palindrome.isPalindrome("racecar");
		assertTrue("Invalid palindrome", result);
	}
	

	@Test
	public void testIsPalindromeException( ) {
		boolean result= Palindrome.isPalindrome("thisfails");
		assertFalse("Invalid palindrome", result);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean result= Palindrome.isPalindrome("race car");
		assertTrue("Invalid palindrome", result);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean result= Palindrome.isPalindrome("race on car");
		assertFalse("Invalid palindrome", result);
	}	
	

	
}
